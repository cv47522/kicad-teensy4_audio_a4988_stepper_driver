EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L teensy:Teensy4.0 U1
U 1 1 5E6A9B4A
P 5580 3730
F 0 "U1" H 5580 5345 50  0000 C CNN
F 1 "Teensy4.0" H 5580 5254 50  0000 C CNN
F 2 "" H 5180 3930 50  0001 C CNN
F 3 "" H 5180 3930 50  0001 C CNN
	1    5580 3730
	1    0    0    -1  
$EndComp
$Comp
L Driver_Motor:Pololu_Breakout_A4988 A1
U 1 1 5E6AF4B0
P 9310 3110
F 0 "A1" H 9360 3991 50  0000 C CNN
F 1 "Pololu_Breakout_A4988" H 9360 3900 50  0000 C CNN
F 2 "Module:Pololu_Breakout-16_15.2x20.3mm" H 9585 2360 50  0001 L CNN
F 3 "https://www.pololu.com/product/2980/pictures" H 9410 2810 50  0001 C CNN
	1    9310 3110
	1    0    0    -1  
$EndComp
NoConn ~ 4480 2680
NoConn ~ 4480 2780
NoConn ~ 4480 2880
NoConn ~ 4480 2980
NoConn ~ 4480 3080
NoConn ~ 4480 3180
NoConn ~ 4480 3280
NoConn ~ 4480 3380
NoConn ~ 4480 3480
NoConn ~ 4480 3680
NoConn ~ 6680 4980
NoConn ~ 4480 5080
NoConn ~ 4480 4980
NoConn ~ 4480 4480
NoConn ~ 4480 4380
NoConn ~ 6680 3480
NoConn ~ 6680 3380
Wire Wire Line
	9310 3910 9410 3910
Wire Wire Line
	9410 3910 9410 3990
Wire Wire Line
	9310 2410 8620 2410
Text Label 8620 2410 0    50   ~ 0
3.3V
Wire Wire Line
	6680 4880 7170 4880
Text Label 7170 4880 2    50   ~ 0
3.3V
Wire Wire Line
	9510 2410 10630 2410
Wire Wire Line
	10630 2710 10630 2410
$Comp
L Device:C C1
U 1 1 5E6AB9FA
P 10630 2860
F 0 "C1" H 10745 2906 50  0000 L CNN
F 1 "100uF" H 10745 2815 50  0000 L CNN
F 2 "" H 10668 2710 50  0001 C CNN
F 3 "~" H 10630 2860 50  0001 C CNN
	1    10630 2860
	1    0    0    -1  
$EndComp
Wire Wire Line
	10630 3010 10630 3990
Wire Wire Line
	10630 3990 9410 3990
Connection ~ 9410 3990
Wire Wire Line
	9410 3990 9410 4150
Text Label 10630 2010 0    50   ~ 0
VOUT
Wire Wire Line
	8910 3010 8280 3010
Wire Wire Line
	8280 3010 8280 3990
Wire Wire Line
	8280 3990 9410 3990
Wire Wire Line
	8910 3110 8520 3110
Wire Wire Line
	8910 3210 8520 3210
NoConn ~ 8910 3410
NoConn ~ 8910 3510
NoConn ~ 8910 3610
NoConn ~ 8910 2710
NoConn ~ 8910 2810
Text Label 8520 3110 0    50   ~ 0
STEP
Text Label 8520 3210 0    50   ~ 0
DIR
Wire Wire Line
	4480 2480 3910 2480
Wire Wire Line
	4480 2580 3910 2580
Wire Wire Line
	4480 3580 4150 3580
Text Label 3910 2480 0    50   ~ 0
DIR
Text Label 3910 2580 0    50   ~ 0
STEP
$Comp
L Connector:Conn_01x04_Female J2
U 1 1 5E6BAA68
P 10170 3110
F 0 "J2" H 10198 3086 50  0000 L CNN
F 1 "Stepper_Conn_01x04_Female" V 10300 2550 50  0000 L CNN
F 2 "" H 10170 3110 50  0001 C CNN
F 3 "~" H 10170 3110 50  0001 C CNN
	1    10170 3110
	1    0    0    -1  
$EndComp
Wire Wire Line
	9810 3010 9970 3010
Wire Wire Line
	9810 3110 9970 3110
Wire Wire Line
	9810 3210 9970 3210
Wire Wire Line
	9810 3310 9970 3310
$Comp
L Connector:Barrel_Jack_Switch J1
U 1 1 5E6C632A
P 7210 1960
F 0 "J1" H 7240 2290 50  0000 R CNN
F 1 "DC_Barrel_Jack" H 7450 2190 50  0000 R CNN
F 2 "" H 7260 1920 50  0001 C CNN
F 3 "~" H 7260 1920 50  0001 C CNN
	1    7210 1960
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6910 1960 6910 2060
Connection ~ 6910 2060
$Comp
L power:GND #PWR01
U 1 1 5E6CBF19
P 6910 2250
F 0 "#PWR01" H 6910 2000 50  0001 C CNN
F 1 "GND" H 6915 2077 50  0000 C CNN
F 2 "" H 6910 2250 50  0001 C CNN
F 3 "" H 6910 2250 50  0001 C CNN
	1    6910 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4480 2380 3910 2380
Text Label 3910 2380 0    50   ~ 0
GND
NoConn ~ 4480 4180
NoConn ~ 4480 3780
NoConn ~ 4480 3880
NoConn ~ 4480 3980
NoConn ~ 4480 4080
NoConn ~ 6680 4280
NoConn ~ 6680 4180
NoConn ~ 6680 4080
NoConn ~ 6680 3980
NoConn ~ 6680 3880
NoConn ~ 6680 3780
NoConn ~ 6680 3680
NoConn ~ 6680 3580
NoConn ~ 6680 3280
NoConn ~ 6680 3180
NoConn ~ 6680 3080
NoConn ~ 6680 2980
NoConn ~ 6680 2780
NoConn ~ 6680 2680
NoConn ~ 6680 2580
NoConn ~ 6680 2880
Wire Wire Line
	6680 4680 7170 4680
Text Label 7170 4680 2    50   ~ 0
VOUT
NoConn ~ 6680 2380
NoConn ~ 6680 2480
$Comp
L Regulator_Linear:L7805 U2
U 1 1 5E6D4102
P 8040 1520
F 0 "U2" H 8040 1762 50  0000 C CNN
F 1 "L7805" H 8040 1671 50  0000 C CNN
F 2 "" H 8065 1370 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 8040 1470 50  0001 C CNN
	1    8040 1520
	1    0    0    -1  
$EndComp
Wire Wire Line
	8040 1820 8040 2200
Wire Wire Line
	8340 1520 8670 1520
Text Label 8670 1520 2    50   ~ 0
5V
Text Label 6910 1030 0    50   ~ 0
9V
$Comp
L power:GND #PWR02
U 1 1 5E6DFFB0
P 9410 4150
F 0 "#PWR02" H 9410 3900 50  0001 C CNN
F 1 "GND" H 9415 3977 50  0000 C CNN
F 2 "" H 9410 4150 50  0001 C CNN
F 3 "" H 9410 4150 50  0001 C CNN
	1    9410 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	10630 2410 10630 2010
Connection ~ 10630 2410
Wire Wire Line
	9510 3910 9410 3910
Connection ~ 9410 3910
Connection ~ 6910 1520
Wire Wire Line
	6910 1520 6910 1030
Wire Wire Line
	6910 1860 6910 1520
Wire Wire Line
	7740 1520 6910 1520
Wire Wire Line
	6910 2200 6910 2250
Wire Wire Line
	6910 2060 6910 2200
Connection ~ 6910 2200
Wire Wire Line
	8040 2200 6910 2200
$EndSCHEMATC
